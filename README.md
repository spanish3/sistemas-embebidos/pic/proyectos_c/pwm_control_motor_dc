# Modulación por Ancho de Pulso (PWM)

La modulación por ancho de pulso (PWM) es una técnica de modulación comúnmente utilizada en sistemas de control electrónico para controlar la potencia entregada a dispositivos eléctricos como motores, LEDs y calentadores. En PWM, la potencia promedio entregada a la carga varía al variar el ancho del pulso en una señal periódica, típicamente una onda cuadrada.

### Cómo funciona PWM:

- **Ciclo de Trabajo:** PWM opera al variar el ciclo de trabajo, que es la relación entre el ancho del pulso (tiempo encendido) y el período de la señal. El ciclo de trabajo determina el voltaje o la potencia promedio entregada a la carga. Un ciclo de trabajo más alto corresponde a un voltaje o potencia promedio más alto, mientras que un ciclo de trabajo más bajo corresponde a un voltaje o potencia promedio más bajo.

- **Frecuencia:** Las señales PWM tienen una frecuencia fija, que es el inverso del período. La frecuencia determina con qué frecuencia se repite la señal PWM dentro de un período de tiempo dado.

![PWM](img/pwm.png)

# Proyecto de Control de Velocidad del Motor con PWM

![Esquemático](img/Schematic.png)

Este proyecto es parte del curso de Circuitos Electrónicos Digitales y Mediciones.

## Tarea del Proyecto
**Tema:** Modulación por Ancho de Pulso  

Desarrollar un proyecto de complejidad media que aplique los conocimientos teórico-prácticos adquiridos en circuitos electrónicos digitales, mediciones e informática electrónica por parte de los estudiantes. El proyecto emplea la técnica de modulación por ancho de pulso para controlar la velocidad de un motor de corriente continua.

## Descripción del Proyecto
- **Objetivo:** Controlar la velocidad de un motor de corriente continua de 12V usando un microcontrolador PIC mediante modulación por ancho de pulso.
- **Hardware y Software:** Desarrollar el hardware y software necesario para controlar la velocidad del motor.
- **Componentes:**
  - Microcontrolador PIC: Se recomienda PIC16F873.
  - Interfaz MOSFET para el control del motor.
  - Regulador de Voltaje: 7805 para la alimentación del microcontrolador.
- **Funcionalidad:**
  - La velocidad del motor aumenta progresivamente con el aumento del voltaje de un potenciómetro de comando.
  - La velocidad del motor se indica mediante un medidor digital de VU de 12 LED con 3 colores.
  - El sistema incluye 3 botones de comando: Iniciar, Freno Progresivo y Freno de Emergencia.
- **Detalles de Diseño:**
  - Utilizar bloques de terminales de paso de 5.08 mm para las conexiones.
  - Disponer los componentes de forma simétrica y ordenada en la PCB de una sola cara.
  - Asegurar una clara etiquetación de los componentes.
  - Utilizar KiCad para el diseño esquemático y de PCB.
- **Requisitos:**
  - Presentar esquemáticos y diseños de PCB claros y ordenados.
  - Incluir un software de microcontrolador bien comentado y explicado.

## Clonar Repositorio

```bash
git remote add origin https://gitlab.com/spanish3/sistemas-embebidos/pic/proyectos_c/pwm_control_motor_dc.git
git branch -M main
git pull origin main