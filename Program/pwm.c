#include "pwm.h"

int1 EA1 = 0, EP1 = 0, EA2 = 0, EP2 = 0;
int8 modo = 0, cont = 0;
int16 aux = 0.0; int16 VAP = 0.0;
void leds();

#int_TIMER2
void TIMER2_isr(void)
{
   // Leer ADC cada 5ms. Esto también compensará el 1 ms que debemos esperar después de leer el ADC.
   VAP = read_adc();

   // Si modo == 1 => Motor funcionando según el potenciómetro => aux = valor de ADC para poder
   // usarlo para mostrar el % de velocidad del motor a través de los LEDs.
   if (modo == 1) { aux = VAP; }

   // Botones presionados detectados:
   if (cont == 0) // Inicialmente, obtener los valores de entrada para ambos botones
   {
      EA1 = input(pin_A1);
      EA2 = input(pin_A2);
   }
   cont++;
   // Esperar 15ms y obtener nuevamente los valores de entrada de los botones.
   if (cont == 2)
   {
      EP1 = input(pin_A1);
      EP2 = input(pin_A2);
      cont = 0;
   }
   // Si el valor cuando cont era 0 es 1 y el valor cuando cont es 2 es 0, entonces se ha presionado un botón.
   if ((EA1 == 1) && (EP1 == 0)) { modo = 1; } // Comprobar botón de INICIO del motor
   if ((EA2 == 1) && (EP2 == 0) && (modo == 1)) { modo = 2; } // Comprobar botón de PARADA progresiva. Solo se puede llegar al modo 2 desde el modo 1
   
   if ((modo == 2) && (cont == 1))   // modo = 2 => PARADA progresiva; cont = 1 => Cada 10 ms
   {
      aux = aux - (VAP / 100); // Con el valor máximo de ADC, aux disminuirá en 10.23 cada 10ms
      if (aux <= 11)      // Por lo tanto, podemos detenernos en 11, para que aux no alcance números negativos.
      {
         aux = 0.0;
         modo = 0;
      }
      set_pwm1_duty(aux);
   }
}

void main()
{
   setup_adc_ports(AN0);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_spi(SPI_SS_DISABLED);
   setup_timer_0(RTCC_INTERNAL | RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_4, 249, 5);  // Desbordamiento 1ms, Interrupción 5ms
   setup_ccp1(CCP_PWM);
   set_pwm1_duty(0);                // RESET => PARADA de emergencia
   enable_interrupts(INT_TIMER2);
   enable_interrupts(GLOBAL);

   set_adc_channel(0);
   delay_us(10);

   // Estado inicial para los indicadores de modo
   output_high(pin_C0); // PARADA
   output_low(pin_C1);  // INICIO del motor
   output_low(pin_C3);  // PARADA progresiva

   // Estado inicial para la visualización del % de velocidad del motor a través de los LED
   output_low(pin_C4);
   output_low(pin_C5);
   output_low(pin_C6);
   output_low(pin_C7);
   output_B(0);

   while (TRUE)
   {
      // Dado que el modo inicial es 0 y el ciclo de trabajo PWM inicial también es 0,
      // el botón de reinicio será el botón de PARADA de emergencia.
      if (modo == 0)              // Motor detenido
      {
         output_high(pin_C0);    // PARADA
         output_low(pin_C1);     // INICIO del motor
         output_low(pin_C3);     // PARADA progresiva
         aux = 0;
      }
      if (modo == 1)              // Motor en marcha / Velocidad según potenciómetro
      {
         output_low(pin_C0);     // PARADA
         output_high(pin_C1);    // INICIO del motor
         output_low(pin_C3);     // PARADA progresiva
         set_pwm1_duty(VAP);     // Establecer el ciclo de trabajo del PWM basado en la lectura del ADC.
         // Con esta configuración, el ADC y el PWM comparten la misma resolución.
         // Esto significa que cuando el voltaje en AN0 es VREF+ (en este caso, 5V), VAP será 1023.
         // Configurar pwm1_duty en 1023 resulta en un ciclo de trabajo del 100%.
         // Por lo tanto, podemos establecer directamente el ciclo de trabajo con el valor leído del ADC.
      }
      if (modo == 2)
      {
         output_low(pin_C0);     // PARADA
         output_low(pin_C1);     // INICIO del motor
         output_high(pin_C3);    // PARADA progresiva
      }
      // Mostrar la velocidad del motor controlando los LEDs, con los estados de los LED determinados por el valor de 'aux'.
      // El valor de 'aux' se establece de manera diferente según el modo actual del sistema:
      // - En modo 1, 'aux' toma su valor del ADC, representando la velocidad del motor.
      // - En modo 0, 'aux' se establece en 0.
      // - En modo 2, 'aux' disminuye progresivamente con el tiempo.
      leds();
   }
}

void leds()
{
   if (aux < 85.25)
   {
      output_low(pin_C4);
      output_low(pin_C5);
      output_low(pin_C6);
      output_low(pin_C7);
      output_B(0);
   }
   if ((aux >= 85.25) && (aux < 170.5))
   {
      output_high(pin_C4);
      output_low(pin_C5);
      output_low(pin_C6);
      output_low(pin_C7);
      output_B(0);
   }
   if ((aux >= 170.5) && (aux < 255.75))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_low(pin_C6);
      output_low(pin_C7);
      output_B(0);
   }
   if ((aux >= 255.75) && (aux < 341))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_low(pin_C7);
      output_B(0);
   }
   if ((aux >= 341) && (aux < 426.25))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(0);
   }
   if ((aux >= 426.25) && (aux < 511.5))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(1);
   }
   if ((aux >= 511.5) && (aux < 596.75))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(3);
   }
   if ((aux >= 596.75) && (aux < 682))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(7);
   }
   if ((aux >= 682) && (aux < 767.25))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(15);
   }
   if ((aux >= 767.25) && (aux < 852.5))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(31);
   }
   if ((aux >= 852.5) && (aux < 937.75))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(63);
   }
   if ((aux >= 937.75) && (aux < 1023))
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(127);
   }
   if (aux >= 1023)
   {
      output_high(pin_C4);
      output_high(pin_C5);
      output_high(pin_C6);
      output_high(pin_C7);
      output_B(255);
   }
}